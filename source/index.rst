.. SPDX-License-Identifier: GPL-3.0-only
.. SPDX-FileCopyrightText: 2020 Vincent Lequertier <vi.le@autistici.org>


CoCitation
==========

The goal is to create a co-citation graph for a list of references.

.. code:: python

   from co_citation import CoCitation

   cites = CoCitation(
       [
           "arxiv:1602.05112",
           "pubmed:8113053",
           "sciencedirect:S0167923610001703",
           "scopus:10.1016/j.cmet.2020.11.014",
       ],
       data_type="journal", # or "article", "institution"
       wait=None, # None or the time to wait between requests (in seconds)
       retries=None, # None or the number of retries for HTTPS requests
       first_last_author=False, # Set to True to only get the institution of the first and last authors
   )
   cites.write_graph_edges("graph")
   cites.plot_graph(
       display=False,
       k=10, # The spacing between the nodes
       seed=42, # Use the seed argument for reproducibility
       margin=dict(b=0, l=110, r=150, t=40)
   )

.. autoclass:: co_citation.CoCitation
   :members:
.. toctree::
   :maxdepth: 2
   :caption: Contents:
