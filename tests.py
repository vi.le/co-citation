# SPDX-FileCopyrightText: 2020 Vincent Lequertier <vi.le@autistici.org>
# SPDX-License-Identifier: GPL-3.0-only

from co_citation import CoCitation
from time import sleep


def test_create_citation_graph():
    cites = CoCitation(
        [
            "arxiv:1602.05112",
            "pubmed:23882417",
        ],
        wait=3,
        data_type="journal",
    )
    assert len(cites.co_citation_graph) == 120


def test_get_journal():
    cites = CoCitation(
        [
            "arxiv:1602.05112",
        ]
    )
    assert (
        cites.get_journal_pubmed(
            "pubmed:23882417",
        )
        == "Healthc Inform Res"
    )


def test_filter_low_co_citation():
    cites = CoCitation(
        [
            "arxiv:1602.05112",
            "arxiv:1703.07771",
            "arxiv:1710.08531",
        ],
        data_type="journal",
    )
    assert len(cites.co_citation_graph) == 95
    cites.filter_low_co_citations(9)
    assert len(cites.co_citation_graph) == 22


def test_filter_low_co_citation_nodes():
    cites = CoCitation(
        [
            "arxiv:1602.05112",
            "arxiv:1703.07771",
            "arxiv:1710.08531",
        ],
        data_type="journal",
        wait=2,
    )
    assert len(cites.co_citation_graph) == 95
    cites.filter_low_co_citations_nodes(0.1)
    assert len(cites.co_citation_graph) == 46


def test_rm_dupes():
    cites = CoCitation(
        [
            "arxiv:1602.05112",
        ]
    )
    assert len(cites.rm_dupes(["test", "test"], 1)) == 1
    assert len(cites.rm_dupes(["test", "testa"], 1)) == 2
    assert len(cites.rm_dupes(["test", "testaaaaa"], 2)) == 2
    assert len(cites.rm_dupes(["test", "testaaaaa"], 6)) == 1


def test_get_article_institution_pubmed():
    cites = CoCitation(
        [
            "arxiv:1602.05112",
        ]
    )
    assert len(cites.get_article_institution_pubmed("23882417")) == 1
    sleep(2)
    assert len(cites.get_article_institution_pubmed("31134379")) == 4
